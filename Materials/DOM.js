
  /*

    DOM нужен для того, чтобы манипулировать страницей –
    читать информацию из HTML, создавать и изменять элементы.

    Всё, что есть в HTML, находится и в DOM.

    document.getElementById -> в контекстке document

    Возращают колекцию могут быть вызваны в контексте
    как документа как и любого элемента

    element.getElementsByTagName
    element.getElementsByClassName

    element.querySelectorAll(css) -> где css любой css selector, вернет колекцию
    element.querySelector(css) -> вернет первое совпадение
    element.matches(css) -> проверка, удовлетворяет ли селектору css, возвращает boolen

    element.closest(css) -> находит ближйший элемент вверх по иерархии DOM который удовлетвореят css

    *document.getElementsByName(name)

    element.innerText('text')
  */

    // document.getElementById('test').style.color = "red";

    // var testNode = document.getElementById('test');
    //     testNode.style.background = "blue";
    //     testNode.innerText = "test";

  //
  // for (var i = 0; i < document.body.childNodes.length; i++) {
  //     console.log( document.body.childNodes[i] ); // Text, DIV, Text, UL, ..., SCRIPT
  //   }
  // console.log( document.body, document.head );
  // console.log( document.body.match('') );

  // document.body.style.background = "red";

  // Выбрать элемент по id и применить стиль
  // id="user" создает переменую user в глобальном обьекте

  // document.getElementById('user').style.background = 'red';
  // user.style.background = 'green';

  // Выбрать элементы по тегу li -> в элементе с id=List
  // 0.098876953125ms
  // 0.44482421875ms


  // var listItems = document.getElementById('list').getElementsByTagName('li');
  //
  // console.log( typeof(listItems), listItems);
  //     listItems[0].style.background = "green";

  // Выборка по css селектору
  // var listItems = document.getElementById('list').querySelector('.test');
  //     console.log( listItems );


  // Проверка элемента по селектору
  // console.log( 'matches', listItems.matches('span') );
  // closest
  // var ClosestItems = document.getElementById('JackLi');
  // console.log( 'closest', ClosestItems.closest('.container'));

  // Выбрать элементы по аттрибуту name
  // var nameItem = document.getElementsByName('Dexter');
  //     console.log( 'name',  nameItem );
