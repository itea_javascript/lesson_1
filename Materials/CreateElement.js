/*
  CREATE ELEMENT
  document.createElement(tag) – создает элемент
  document.createTextNode(value) – создает текстовый узел

  PASTE ELEMENT
  parent.appendChild(element)
  parent.insertBefore(element, nextSibling)

  REMOVE ELEMENT
  parentElement.removeChild(element);
  element.remove();

*/
