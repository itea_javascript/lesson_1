/*
BOM ->
window.
      navigator, https://developer.mozilla.org/ru/docs/Web/API/Navigator
      history, https://developer.mozilla.org/ru/docs/Web/API/History
      screen, https://developer.mozilla.org/ru/docs/Web/API/Screen
      location, https://developer.mozilla.org/ru/docs/Web/API/Location
      document -> DOM https://developer.mozilla.org/ru/docs/Web/API/Document
      alert, prompt, console,

*/


// console.log('Hi console' , {name: '1val1'}, ['1','2']);
// console.table({name: 'value1', name2: 'vasya'});
// console.table(["apples", "oranges", "bananas"]);
// console.error('Some error in obj:', { name: 'Cat'});
// console.warn(' SOME WARNING');
//
// console.log("This is the outer level");
// console.group();
// console.log("Level 2");
// console.group();
// console.log("Level 3");
// console.warn("More of level 3");
// console.groupEnd();
// console.log("Back to level 2");
// console.groupEnd();
// console.log("Back to the outer level");

// console.time('fetch');
// var fetchedData = fetch('http://www.json-generator.com/api/json/get/bQnzYgqhua?indent=2').then(function(response) {
//  return response.json();
// }).then(data => {
//    console.timeEnd('fetch');
//    console.log('data', data);
// });


// console.log( navigator );
// console.log( location );
// location.href = "http://google2312312.com.ua";
// console.log( location.hash );
// console.log( screen.width, screen.height, screen );
//
// window.history.pushState({userlink: 'fblink', name:'sad'}, '', '#link2');
// // //
// numberOfEntries = window.history.length;
// console.log(numberOfEntries);
// console.log( 'message before', window.history.state.userlink );
//
//
// window.history.pushState({message: 'BOOM'}, '', '#link2');
// console.log( 'message after', window.history.state.message );
